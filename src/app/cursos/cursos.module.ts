import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PrincipalComponent} from './pages/principal/principal.component';
import {ComprarComponent} from './pages/comprar/comprar.component';
import {ListadoComponent} from './pages/listado/listado.component';
import {CursoComponent} from './pages/curso/curso.component';
import {BuscarComponent} from './pages/buscar/buscar.component';
import {CursosRoutingModule} from "./cursos-routing.module";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MaterialModule} from "../material/material.module";
import {CursoCardComponent} from './components/curso-card/curso-card.component';
import {ConfirmarComponent} from './components/confirmar/confirmar.component';


@NgModule({
  declarations: [
    PrincipalComponent,
    ComprarComponent,
    ListadoComponent,
    CursoComponent,
    BuscarComponent,
    CursoCardComponent,
    ConfirmarComponent
  ],
  imports: [
    CommonModule,
    CursosRoutingModule,
    FlexLayoutModule,
    MaterialModule
  ]
})
export class CursosModule {
}
