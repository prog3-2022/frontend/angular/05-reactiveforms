export interface Curso {
  id?: number;
  nombre: string;
  url: string;
  imagen?: string;
  cantHoras: number;
  precio: number;
  categoria: string;
  descripcion: string;
}
